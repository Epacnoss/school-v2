---
tags: ["Revision", "Chemistry", "Chemistry/InorganicChem", "TopicQuestions"]
subject: Chemistry
topic: InorganicChem
year: U5
revision: true
marked: true
---

## 1
#### c
To remove any traces of solution X.
? **to remove any water**

#### d
Solution Y is less reactive. **less concentrated**


## 1
#### a
The polystyrene cup is a better insulator.

#### b
15.9
19.4
3.5

#### d
ΔQ = 4.2 \* mass \* ΔT
 = (25 + 22.7) \* 4.2 \* (24.7 - 18.9)
 = 47.7 \* 4.2 \* 5.8
 = 1161.972
 = 1160J


## 3
#### a
measuring column **pipette**

#### b
A **Pink to colourless**

#### c
Phenolphthalein changes with no gradient, allowing you to quickly stop as soon as it is neutral, rather than judging what colour is neutral.

#### d
2.3
24.15
21.85

#### e
###### i
27.65, 27.80, 27.75 **only 26.3, 26.4**
1.00, 1.00
26.30, 26.40
###### ii
 = ((27.65 + 27.8 + 27.75) / 3) - ((1.0 + 1.0) / 2)
 = 27.73 - 1
 = 26.73cm^3 **ecf**

#### f
###### i
**dm^3 is 1000cm^3, not 100cm^3**
 = 25 / 100 \* 0.18
= 4.5 **no lol = 0.045**
###### ii
 = 4.5 / 3
 = 1.5 mol **ecf**
###### iii
 = 1.50 / (28.3 / 100)
 = 5.3 **ecf**


## 4
#### a
22.10, 22.20

#### b
22.15

#### c
C **D - pipette**

#### d
###### i
 = (20 / 100) \* 0.02 **/ 1000**
= 0.004 mol
###### ii
 = 0.004 \* 5
 = 0.02 mol
###### iii
 = 0.2 mol
 ###### iv
 = 0.02 \* 152
 = 3.04g

#### e
###### i
 = 24.2 - 15.2
 = 9
###### ii
RAM<sub>H<sub>2</sub>O</sub> = 2 + 16
 = 18

 = 9/18
 = 0.5 mol
###### iii
 = 15.2 / 152
 = 0.1 mol
###### iv
x = 5