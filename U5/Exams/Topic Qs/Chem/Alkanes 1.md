---
tags: ["Revision", "Chemistry", "Chemistry/OrganicChem", "TopicQuestions"]
subject: Chemistry
topic: OrganicChem
year: U5
revision: true
marked: true
---

## 1
#### a
###### i
QRST **QRSP, so close**
###### ii
Hydrogen, Magnesium Chloride

#### b
Electrolyse the Hydrochloric Acid, and above the Anode, put some damp litmus paper. It should go red, as the chlorine gas from the electrolysis reacts with the water in the litmus paper, forming hydrochloric acid.
**Maybe, but add AgNO<sub>3</sub>, white precipitate**


## 2
#### a
All of these are hydrocarbons as they only contain Hydrogen and Carbon.

#### b
Because they contain Carbon=Carbon double bonds.

#### c
A

#### d
B, E, **F**

#### e
F is a saturated hydrocarbon, and bromine only reacts with unsaturated hydrocarbons.

#### f
###### i
| _         | C     | H     | Br      |
| --------- | ----- | ----- | ------- |
| **Mass**  | 22.2  | 3.7   | 74.1    |
| **RAM**   | 12    | 1     | 80      |
| **Moles** | 37/20 | 37/10 | 741/800 |
| **Ratio** | 2     | 4     | 1       |

##### ii

RAM C<sub>2</sub>H<sub>2</sub>Br = 2\*12 + 4\*1 + 80 = 108
RMM = 216
216 / 108 = 2

C<sub>4</sub>H<sub>8</sub>Br<sub>2</sub>


## 3
#### a
###### i
Fuel Oil
###### ii
Kerosene
###### iii
Gasoline

#### b
###### i
Phosphoric Acid (H<sub>2</sub>PO<sub>3</sub>)
###### ii
2C<sub>4</sub>H<sub>8</sub>
###### iii
To get more useful products as the long-chain molecules are less useful.

#### c
This compound contains sulphur, so when it is combusted, it could produce Sulphur Dioxide, which is dangerous to breathe in (as it reacts with the moisture in the lungs to product Sulphuric Acid), and can cause Acid Rain.

#### d
??


## 4
#### a
###### i
C<sub>5</sub>H<sub>12</sub>
###### ii
CH<sub>2</sub>Br

#### b
###### i
P, Q
###### ii
B

#### c
A
#### d
Can do fine
#### e
###### i
??
###### ii
??
###### iii
??
###### iv
??
#### f
###### i
| \_        | C    | H   | F    |
| --------- | ---- | --- | ---- |
| **Mass**  | 36.4 | 6   | 57.6 |
| **RAM**   | 12   | 1   | 19   |
| **Moles** | ¬3   | 6   | ¬3   |
| **Ratio** | 1    | 2   | 1     |

CH<sub>2</sub>F
###### ii
RMM CH<sub>2</sub>F = 12 + 1 + 19 = 33
66 / 33 = 2

C<sub>2</sub>H<sub>4</sub>F<sub>2</sub>