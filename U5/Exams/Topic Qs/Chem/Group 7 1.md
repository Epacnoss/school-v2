---
tags: ["Revision", "Chemistry", "Chemistry/InorganicChem", "TopicQuestions"]
subject: Chemistry
topic: InorganicChem
year: U5
revision: true
marked: true
---

[not marked, but no longer taking subject]

## 1
#### a
D

#### b
180

#### c
Black
Solid

#### d
C

#### e
decreases, increases
positive, negative
reducing, oxidising


## 2
#### a
C, D

#### b
###### i
A

###### ii
Hydrochloric Acid has a lower relative formulaic mass than Ammonia, so can diffuse faster and reach the ammonia faster.


## 3
#### a
###### i

| -         | C               | Fl              | Cl              |
| --------- | --------------- | --------------- | --------------- |
| **Mass**  | 0.24            | 0.38            | 1.42            |
| **RAM**   | 12              | 19              | 35.5            |
| **Moles** | 50<sup>-1</sup> | 50<sup>-1</sup> | 25<sup>-1</sup> |
| **Ratio** | 1               | 1               | 2               |

Must be CFCl<sub>2</sub>
###### ii
CFCl<sub>2</sub> = 12 + 19 + 35.5 \* 2
 = 102
204 / 102 = 2

 = C<sub>2</sub>F<sub>2</sub>Cl<sub>4</sub>


## 4
#### a
gas, red-brown, solid

#### b
###### i
Mg<sup>+</sup>
Cl<sup>-</sup>
###### ii
Chlorine Gas
Damp litmus paper is bleached.
###### iii
???
###### iv
???