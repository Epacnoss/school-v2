---
tags: ["Revision", "Chemistry", "Chemistry/OrganicChem", "TopicQuestions"]
subject: Chemistry
topic: OrganicChem
year: U5
revision: true
marked: true
---

## 1
#### a
###### i
Only contains carbon-carbon single bonds.
###### ii
Only contains hydrogen and carbon atoms.
###### iii
C

#### b
###### i
4C<sub>8</sub>H<sub>18</sub> + 41O<sub>2</sub> -> 32CO<sub>2</sub> + 18H<sub>2</sub>O
**C<sub>8</sub>H<sub>18</sub> + 12.5O<sub>2</sub> -> 8CO<sub>2</sub> + 9H<sub>2</sub>O**

###### ii
Carbon Monoxide

#### c
###### i
?? **lower activation energy**
###### ii
?? **aluminium oxide**
###### iii
C<sub>2</sub>H<sub>4</sub>
###### iv
Ethene


## 2
#### a
###### i
A
###### ii
C
###### iii
C

#### b
C<sub>4</sub>H<sub>10</sub>, C<sub>2</sub>H<sub>5</sub>

#### c
###### i
Alkanes, C<sub>n</sub>H<sub>2n + 2</sub>

#### d
Not enough Oxygen in the reaction, so Carbon Monoxide produced instead of dioxide, which can form carboxyhaemoglobin in the blood, which reduces the oxygen absorbt in the blood. This can reduce the oxygen flowing to the brain.


## 3
#### a
Turning one long-chain hydrocarbon into multiple shorter hydrocarbons.
#### b
dissolve that gas in bromine water, goes orange->colourless
#### c
Phosphoric Acid (H<sub>3</sub>Po<sub>4</sub>), 300&deg **600-700&degC**


## 4
#### a
One molecule with the same chemical formula can be arranged in different ways

#### b
D - just the same thing

#### c
###### i
C<sub>n</sub>H<sub>2n</sub>

#### d
Addition?, Orange, Colourless **yes**

#### e
Heptane contains only single bonds and no room for other atoms, but Heptene contains double bonds and so other atoms can bond.


## 5
#### a
###### i
CH<sub>4</sub>
###### ii
C<sub>2</sub>H<sub>6</sub>
###### iii
CH<sub>2</sub>-CH<sub>2</sub>-CH<sub>2**3**</sub>

#### b
###### i
Alkanes
###### ii
C<sub>n</sub>H<sub>2n + 2</sub>
###### iii
Differ by CH<sub>2</sub>, Similar chemical properties