---
tags: ["Revision", "Chemistry", "Chemistry/OrganicChem", "TopicQuestions"]
subject: Chemistry
topic: OrganicChem
year: U5
revision: true
marked: true
---
## 1
#### a
The graph didn't plateau at the end.

#### b
The polystyrene cup is a good insulator

#### c
###### i
Effervescense of the CO<sub>2</sub>
###### ii
1) ? **neutralisation**
2) ?  **endothermic**

#### d
Up and Down below the original line. Δ E/H labeled. Reactants/Products Labeled.

#### e
###### i
ΔQ = mcΔT = 0.1l \* 4.2 \* (18.7 - 13.2)
       = 2.31J
**use ml**
###### ii
The concentration of the vinegar