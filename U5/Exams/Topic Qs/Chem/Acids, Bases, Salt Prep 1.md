---
tags: ["Revision", "Chemistry", "Chemistry/InorganicChem", "TopicQuestions"]
subject: Chemistry
topic: InorganicChem
year: U5
revision: true
marked: true
---

## 1
#### a
###### i
Effervescence,
pH goes down **calcium solid disappears**
###### ii
Calcium Hydroxide is basic **and give a pH - anything gte 8**

#### b
CaO
CaCl
?? **CaCO<sub>3</sub>**


## 2
#### a
###### i
Ca, Mg
###### ii
Fe
###### iii
Ca
Mg
Zn
Fe
Cu


#### b
###### i
Hydrogen
###### ii
All of the sulphuric acid has been reacted
###### iii
Magnesium Sulphate
###### iv
Filtration

#### c
###### i
Exothermic
###### ii
Magnesium Oxide


## 3
##### a
###### i
Silver chloride
###### ii
AgNO<sub>3</sub> + NaCl -> AgCl + NaNO<sub>3</sub> 

#### b
s, ag, s, ag **aq, not ag**, **item no 3 should still be aq**

#### c
###### i
Sodium Chloride **hydroxide**
Nitric Acid
###### ii
3.6 **should be below**
22.3 **should be ^**
18.7
**right readings wrong order**

#### d
###### i
-
0.80, 0.60
23.55, 22.50, 22.60 **not 22.6**
###### ii
 = (23.55 + 22.5 + 22.6) / 3
 = 22.88 **ECF**

#### e
**filter THEN, **Continue heating in a desiccator.

#### f
RAM<sub>2NaNO<sub>3</sub></sub> = 2 \* 85 = 170
RAM<sub>2NaNO<sub>2</sub></sub> = 2 \* (23 + 14 + 16 \*2) = 2 \* (85 - 16) = 138

mol<sub>2NaNO<sub>3</sub></sub> = 1.7 / 170 = 0.1
mass<sub>2NaNO<sub>2</sub></sub> = 0.1 \* 138 = 1.38g