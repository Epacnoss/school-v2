---
tags: ["Revision","Chemistry","Chemistry/OrganicChem", "TopicQuestions"]
subject: Chemistry
topic: OrganicChem
year: U5
revision: true
marked: true
---

## 1
#### a
A

#### b
D **A**

#### c
###### i
Propene

#### e
###### i
They naturally degrade in a relatively short period of time without need for exposure to other chemicals. **mention bacteria,micro-organisms, etc.**
###### ii
Large number of bonds? **unreactive**


## 2
#### a
###### ii
A series of repeating monomers. **long chain; formd when many monomers join together**
###### iii
Polyfluroethene

#### b
Ethene, C<sub>2</sub>H<sub>4</sub>

#### c
They take a very long time to degrade. **do not degrade because unreactive**


## 3
#### a
\_, Plastic Bag
? **bucket, gas pipes, etc.**

#### b
Break the double bond from the alkane,
? **monomers join; increase chain size**

#### c
###### i
? **same molecular formula**
###### ii
Creates toxic fumes
Can be dangerous to animals who might choke if the plastic floats away


## 4
#### a
Contains only single bonds

#### b
###### i
C<sub>8</sub>H<sub>18</sub> + C<sub>2</sub>H<sub>4</sub>
###### ii
Either vaporised long-chain hydrocarbons over powedered aluminium oxide, or steam at high temperature (300&degC **600-700**) and pressure (70-100atm)

#### c
###### i
They have the exact same chemical formula, but different structures.

#### d
###### i
Polypropene


## 5
#### a
The graph had not yet plateaued

#### b
Better insulator than the glass

#### c
###### i
Effervescense of CO<sub>2</sub>
###### ii
1) Neutralisation
2) Endothermic


#### e
###### i
ΔQ = mcΔT
       = 100 \* 4.2 \* (18.7 - 13.2)
	   = 2310J
###### ii
The concentration


## 6
#### a
###### i
Contains only hydrogen and carbon
###### ii
C<sub>10</sub>H<sub>22</sub>

#### b
###### i
Addition
###### ii
Breaking C=C double bonds
? **monomers join; increase chain size**

#### c
Crude Oil contains lots of hydrocarbons, and some short-chain hydrocarbons like Methane are very useful as they are very combustible. However, there are also lots of long-chain hydrocarbons (40 carbons are more), which are less useful. By cracking long-chain hydrocarbons, we can make better use of them as short-chain hydrocarbons.


## 7
#### a
H<sub>2</sub>

#### b
Only single bonds.

#### c
###### ii
D **C - colourless**

#### f
###### i
They naturally degrade in a relatively short period of time.  **mention bacteria,micro-organisms, etc.**
###### ii
?? **unreactive**