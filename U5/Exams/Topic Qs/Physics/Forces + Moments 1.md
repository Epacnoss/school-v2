---
tags: ["Revision", "Physics", "Physics/ForcesAndMotion", "Physics/ForcesAndMotion/Moments", "TopicQuestions"]
subject: Physics
topic: Forces and Motion
year: U5
revision: true
marked: true
---

QP: [[3.1_-_forces_movement_shape_and_momentum__1p__-_edexcel_igcse_physics_qp.pdf]]


## 1
#### a
A ruler.

#### b
The student can repeatedly stretch the spring to lots of different distances and measure how much force it takes to stretch it to that position. Then, they can plot the results on a graph. The gradient should be constant, and if it isn't then the spring isn't following Hooke's law.
[what more?]
**stuff like it going through the origin, and doing it the OWA - put on weight and measure the length**


## 2
#### a
###### i
$KE = \frac{mv^2}{2}$
###### ii
$KE = \frac{mv^2}{2}, v = \sqrt{\frac{2*KE}{m}} = \sqrt{\frac{18,000,000 * 2}{250,000}} = \sqrt{144} = 12m/s$
###### iii
As the velocity decreases, so does the kinetic energy. **transferred to surroundings**

#### b
###### i
$GPE = mgh$, so if they start below the ground, $h$ is negative and so they must have a negative $GPE$. When they then rise up, $h$ increases to $0$, taking the $GPE$ to $0$, which is an increase from the previous negative value. Therefore, they must gain GPE.
###### ii
If the train must go up a slope, then it must do work against gravity, reducing the Kinetic Energy. This means that less force is needed from the brakes as gravity does some work. The reverse happens as the train leaves the station, where the motor needs to do less work as gravity helps the train down.


## 3
#### a
9100

#### b
###### i
$F = ma$
###### ii
$F = ma, a = F/m = 400 / 910 = 0.4395604396 = 0.44m/s^2$

#### c
???
**speed inc; drag inc; downward force inc; acc dec**

#### d
As the sand leaves the bags, the mass decreases, increasing the acceleration.


## 4
#### a
###### i
6
###### ii
10

#### b
###### i
$A = \frac{{\Delta}V}{{\Delta}T}$
###### ii
$A = 12/10 = 1.2m/s^2$

#### c
###### i
$S = \frac{D}{T}$
###### ii
$S = 390/60 = 6.5m/s$

#### d
The area under the first hump is bigger?


## 5
#### a
1) Use Callipers
2) ? **vertical ruler. remove parallax etc.**

#### b
###### i
Distance
###### ii
By dividing the mass (in grams) by 100.


## 6
#### a
###### i
The weight of the car.
###### ii
The speed of the car.

#### b
1) Ramp material
2) Total ramp length

#### c
Stopwatch, newton meter

#### d
[done 1x10^6 times]