---
tags: ["Revision", "Physics", "Physics/MagnetismAndElectromagnetism", "Physics/MagnetismAndElectromagnetism/EMInduction", "TopicQuestions"]
subject: Physics
topic: Magnetism and Electromagnetism
year: U5
revision: true
marked: true
---

[[23_-_electromagnetic_induction_1p__-_edexcel_igcse_physics_qp.pdf]]

## 1
#### a
As the rod connects with the negative and the positive terminals, it will conduct electricity. 
As the electricity in the rod interacts with the magnetic fields from the row of magnets, a force will be applied on the rod, 
pushing it back to the terminals. 
This direction is given by Fleming's left hand rule.

#### b
As an alternating current flows through the coil, it induces a magnetic field. This magnetic field interacts with the magnets, and induces a force on the coil and the speaker cone. As the current is alternating, that force will rapidly switch directions, causing the coil to move left and right rapidly, shaking the loudspeaker cone. As the loudspeaker cone shakes, it will vibrate the air molecules, creating a pressure wave interpreted as sound.


## 2
#### a
1) Switch the magnet to have the North Pole going downwards.
2) Swap the positive and negative terminals on the ammeter.

#### b
###### i
| Letter | Name         |
| ------ | ------------ |
| Y      | Magnet       |
| Z      | Coil of Wire |
###### ii
1.6V
###### iii
 = 1 / 0.04
 = 25Hz
###### iv
C
###### v
Make the coils of wire have more loops.

#### c
###### i
 = 3.1W \* 290s
 = 899J
###### ii
Efficiency = Useful / Total
###### iii
 = 899 / 0.72
 = 1248.6111 J
 = 1250J