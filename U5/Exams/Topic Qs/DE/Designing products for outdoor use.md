---
tags: ["Revision", "DesignEngineering", "DesignEngineering/Design", "TopicQuestions"]
subject: DesignEngineering
topic: Design
year: U5
revision: true
marked: true
---

QP: [[Designing products for outdoor use (6).pdf]]
MS: [[Designing Products for outdoor use MS (6).pdf]]

There are plenty of considerations when designing for outdoors use.

Firstly, the weather. If your product is designed to be used outside, then it is more likely to be exposed to adverse weather conditions. If these include lots of rain (especially near the sea side), you must account for rusting, and so avoid any ferrous alloys that might rust. Rain and rust can also be incredibly damaging to microelectronics, so you must make sure to place all electronics in a rain-proof area to avoid damage. If your product is being used in an area that gets storms, you must make sure that all of the electronics are encased in an insulator or EMI-proof. If you are building a product for use in a desert, you must make sure that condensation will not destroy your product.

Next, people and animals. If your product is being used in a public place, you must make sure that it is tamper-proof and will not be disturbed by any animals.

??

> - **talk about treatments**
> - **talk about woods + expansion/contraction**
> - **maybe mention plastics more durable**
> - **specific egs**