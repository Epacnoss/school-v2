---
tags: ["Revision", "DesignEngineering", "DesignEngineering/CAD", "TopicQuestions"]
subject: DesignEngineering
topic: CAD
year: U5
revision: true
marked: true
---

QP: [[CAD vs Traditional methods (6).pdf]]
MS: [[CAD vs Traditional Methods Example.pdf]]

There are lots of different advantages to using CAD for modern designers, both for design and manufacture.

Firstly, when using CAD designers can easily change dimensions if something turns out to be too big or small or if the client would like something different. They wouldn't be able to do this as easily on paper because they would have to rub out the lines and redraw them, which both fatigues the paper and is a very slow process. Next, is that there are lots of different technologies to see different views of their products, if they want to see a different perspective. They can also use other modern technologies like Virtual Reality in conjunction with their CAD software, in order to make sure that all of their scales are correct.

There are also advantages in manufacture from using CAD. Many modern machines (eg. CNC mill, 3D printer, laser cutter) require the use of CAD to provide the instructions for manufacture, so even designs start on paper, they will eventually have to be converted to CAD. Also, using digital CAD files ensures that there can't be any miscommunications between designers and manufacturers.

**pretty good and similar to the model answer**