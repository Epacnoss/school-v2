---
tags: ["Revision", "DesignEngineering", "DesignEngineering/CAD", "TopicQuestions"]
subject: DesignEngineering
topic: CAD
year: U5
revision: true
marked: true
---

QP: [[CAM CNC vs Hand Tools (6).pdf]]
MS: [[CAM CNC vs Hand Tools MS (6).pdf]]

There are lots of advantages and disadvantages both ways to using Computer Aided Manufacture vs using hand tools, here I'll talk about 3 main topics - expenses, time, and ease of use.

If you are manufacturing your product using CAM, this can often be a lot more expensive, and so is harder for new people. The machines are also often very complicated, making them difficult to repair without specialty technicians who might take a while to arrive as well as costing a lot of money. On the other hand, hand tools are often comparatively much cheaper and simpler, meaning it is easier to get started, they are less likely to break and easier to fix when they do break.

For time, CAM can be a lot faster, assuming you already have CAD files at the ready. This is because the CNC machines don't need to mark out parts, and they can cut intricate patterns that would be difficult for a human with hand tools with ease. They also rarely make mistakes. However, using hand tools can be a lot faster if you don't have CAD files or intricate designs as you don't need to make the CAD files, and since you have a physical model in front of you, you can make changes to the design in real time.

For ease of use, CAM can be quite good as you don't need experience and you can easily learn how to create CAD files away from a workshop which you cannot do for traditional hand tools.

**:poggers:**
