---
tags: ["Revision", "DesignEngineering", "DesignEngineering/Wood", "TopicQuestions"]
subject: DesignEngineering
topic: Wood
year: U5
revision: true
marked: true
---

QP: [[Marking out a wood join.pdf]]
MS: [[Marking out a wood join MS.pdf]]
?????

**4 of:**

> 1) mark centre line of cut end 9.5mm from face
> 2) mark centres of 2-3 holes along centre line
> 3) drill suitable holes at marked points diam 6-8mm
> 4) insert centre-point tool into holes
> 5) mark line across top of side, 9.5mm down from top edge
> 6) line up top with side and tap side to mark hole centres
> 7) drill holes to suitable depth (< 18mm deep)
> 8) mark length of dowel
> 9) tenon saw to cut dowels to length
> 10) dry-fit joint
> 11) PVA/wood glue to protruding dowels and assemble in place.