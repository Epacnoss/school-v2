---
tags: ["Revision", "DesignEngineering", "DesignEngineering/Sustainability", "TopicQuestions"]
subject: DesignEngineering
topic: Sustainability
year: U5
revision: true
marked: true
---

QP: [[Transporting large goods (6).pdf]]
MS: [[Transporting large goods MS (6).pdf]]

There are lots of environmental implications of sending assembled products around the world. I'll talk about the modes of transport and the products themselves.

Firstly, the modes of transport. If the products need to be transported quickly, they might be flown via plane which is very bad for the environment as planes use a lot more fuel than other modes of transport. If the transport time is less important or the transport volume is more important, then a cargo ship might be used. These have a smaller environmental impact, but lots of carbon is still released. There is also the additional issue that boats are more likely to crash than planes, and boats often carry cargo that you might not want to be in the sea. Lots of oil spills that have been incredibly damaging for the local wildlife have originated from cargo ships carrying oil that have crashed into rocks.

Next, the products themselves. Due to the massive scales of manufacture that are frequently used, extremely minimal changes can cause incredible changes in profits and losses. For example, Iceland is a massive centre for aluminium production due to the cheap power costs (as electrolysis uses a lot of power), and lots of Bauxite is refined there, even if it has to come from very far away. The fully assembled products also provide a problem. Regardless of how much cargo they are carrying, cargo ships will never use less than a threshold of fuel to move the ship itself. This dictates that to maximise the fuel efficiency on a cargo ship you want to pack it as full as possible, but fully assembled and packaged products are often mostly air. This is bad for the environment, as it means that more cargo ships are needed than if the products had been shipped flat-packed and assembled on arrival.


> - **large packages more wasteful when disposed of**
> - **help lifestyle of LEDC people**

**pretty good tho**
