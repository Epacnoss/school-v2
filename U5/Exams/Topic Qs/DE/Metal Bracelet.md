---
tags: ["Revision", "DesignEngineering", "DesignEngineering/Metals", "TopicQuestions"]
subject: DesignEngineering
topic: Metals
year: U5
revision: true
marked: true
---

QP: [[Metal Bracelet QP.pdf]]
MS: [[Metal Bracelet MS.pdf]]

## 17
#### a
Aluminium

#### b
1) High strength-to-weight ratio
2) Will not rust.
**4 marks - must also provide explanation**

#### c
###### i
5mm **rip - 0.5 to 2**
###### ii
Anthropometric data could be used to find the 90th percentile for wrist circumferences, and you could make it slightly shorter than that. **so the bracelet fits**

#### d
Tin Snips, Hand Drill, Sandpaper/File