---
tags: ["Revision", "Biology", "Biology/Reproduction&Inheritance", "Biology/Reproduction&Inheritance/Inheritance", "TopicQuestions"]
subject: Biology
topic: "Reproduction and Inheritance"
year: U5
revision: true
marked: true
---

# 1
## a
The purple phenotype was dominant, because it appeared in all of the offspring.

#### b
He covered the stigma, and pollinated the plants by hand.

#### c
###### i
Ff Ff
F f **F f**
FF Ff ff
purple purple white
###### i
 = 8/36 = 2:9 **other way around dummy**
###### ii
Nature has an element of randomness that will conform to expectations in a larger sample, but not a smaller one.

#### d
 - More of the purple flowers survive as they can reproduce more often
 - This increases the proportion of purple flowers. **over multiple generations**


## 2
DNA, Nucleus, Chromosomes, T**hymine**, Guanine, mutation


## 3
#### a
Tube 3 at 30&degC.
Possibly the test tube wasn't cleaned properly after the 25&degC experiment, and some offspring remained.

#### b
###### i
lol no
###### ii
22
Tube 4 at 35&degC

#### c
They are all close to each other? **yeah, just say repeated etc.**

#### d
The offspring in relation to temperature follows a bell curve, peaking at 25&degC, with 462 total offspring, decreasing very fast going colder, and decreasing slowly going warmer, and stopping at 45&degC.

The offspring survive best at 25&degC, but can function colder. They cannot go hotter than around 45&degC.


## 4
#### a
Father - Woman carries XX, Father carries XY, so egg always contains X, but sperm can contain X or Y.

#### b
??
**1) produces 4 cells
2) produces haploid cells
3) halves chromosome number
4) produces genetic variation
5) produces gametes
6) takes place in sex organs**


## 5
??
**1) colder the place bigger the mouse
2) variation
3) due to mutation
4) bigger mice survive
5) less heat loss
6) smaller SA:V ratio
7) pass on allele**

## 6
#### a
2, 11, 3 **4**, 0

#### b
XX XY
female male
X X Y Y
XX XY
female male