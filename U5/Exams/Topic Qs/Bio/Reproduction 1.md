---
tags: ["Revision", "Biology", "Biology/Reproduction&Inheritance", "Biology/Reproduction&Inheritance/Reproduction", "TopicQuestions"]
subject: Biology
topic: "Reproduction & Inheritance"
year: U5
revision: true
marked: true
---
[not marked, but no longer taking subject]

[[13.1-reproduction-1b-igcse_9-1_-edexcel-biology.pdf]]

## 1
#### a
1) Plants have flowers in order to encourage insects to come
2) Plants produce nectar in order to encourage insects to come
3) No casing for insect-pollinated flower pollen.

#### b
A - Flower
B - Stigma
C - Style

#### c
 - Pollen lands on style
 - Pollen tube grows down the style 
 - Pollen tube enters the style
 - Pollen tube uses microtubules