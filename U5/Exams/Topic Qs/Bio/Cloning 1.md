---
tags: ["Revision", "Biology", "Biology/UseOfBiologicalResources", "TopicQuestions"]
subject: Biology
topic: "Use of Biological Resources"
year: U5
revision: true
marked: true
---

## 1
#### a
Blank
**1) explants; 2) small pieces of tissue; 3) agar; 4) contains nutrients; 5) plant hormones; 6) minerals; 7) sterile; 8) control light; 9) control humidity**

#### b
1) Takes less time
2) More plants at once


## 2
- Get sperm from 'parent' animal
- Get egg from 'donor' animal
- Enucleate egg to remove genetic material
- Add genetic material from sperm
- Shock egg to begin fusion.
- Implant egg into surrogate animal
**Mention uterus/womb, and embryo**


## 3
#### a
Lung, Heart, Pancreas, Liver, Corneas

#### b
The liver filters the blood?
**Bile emulsifies, to neutralise stomach acid**

#### c
###### i
An organ that is cloned from another organ, rather than extracted from a donor. **GENETICALLY IDENTICAL**
###### ii
No need for people to donate organs, Can fufill specific niche requirements regarding shapes or blood types


## 4
 - We are looking for the most unfertilised eggs to survive in different temperatures,
 - so the scientists should get 20 eggs for each temperature bracket, and there should be a variety of ranges (eg. 16&deg->20^deg, 21&deg->25&deg, etc.)
 - The scientists should make sure to repeat the whole experiment 5 times, and take an average of the results to reduce the possibility of a mistake affecting the results.
 - After the time period, the scientists can check how many eggs survived.
 - They need to have all the eggs from the same species of animal, and the nutrient solution needs to be identical for each batch (ignoring the temperature)


## 5
identical,explant,sterilised,**micro-organisms (you must have missed it**,growing,glucose,chlorophyll,nitrates,large,any


## 6
Boring! **0 marks lol**


## 7
#### a
6, 7, 3

#### b
P **C (R)**

#### c
D