---
tags: ["Revision", "Biology", "Biology/UseOfBiologicalResources", "TopicQuestions"]
subject: Biology
topic: "Use of Biological Resources"
year: U5
revision: true
marked: true
---


## 1
#### a
Once the farmers identify cows who produce lots of milk, they can breed them with other cows to increase the number of cows and the strength of that characteristic.

**1) select high milk yield cow; 2) breed; 3) use offspring w/ high milk yield; 4) repeat**


#### b
###### i
The Womb
###### ii
Much faster, guarantee of characteristics, no need to buy lots of expensive sperm **no variation, no need for 2 parents**

#### c
Produce a genetically identical copy

#### d
Mitosis


## 2
#### a
Blank
**1) human allele; 2) restriction enzyme; 3) plasmid; 4) vector; 5) same restriction enzyme; 6) ligase; 7) recombinant plasmid/dna**

#### b
###### i
A chemical that travels in the body via the bloodstream, is excreted by glands, is slow to have an effect, and has a long-lasting effect.
###### ii
 - Identify cows that produce lots of milk
 - Seperate them from the rest of the population
 - Breed them with other cows
 - Continue over and over with the best milking cows
 - Increase milk production over time.


## 3
#### a
###### i
 - Identify plants with the desired characteristics
 - Seperate them from the other plants
 - Pollinate those plants with their own pollen
 - Continue with the 'children' plants that have the same characteristic
 - The characteristic grows stronger
###### ii
Jucier Fruit, Tomato Plant

#### b
1) Natural Selection works on what is best for the plant, rather than the consumer.
2) Selective Breeding is much faster


## 4
#### a
I can draw graphs

**S: scale linear, use at least half grid; L: Lines on bars neat; A: Axes correct way; P: Bars at correct height**

#### b
1) Sunlight Exposure
2) Water Access
3) External Temperature

#### c
###### i
1) Farmers are more likely to use fertilisers to increase crop growth
2) Farmed Fields have no competition for growth, whereas crops in grassland could have pests, or other types of non-useful plants growing (eg. weeds)
###### ii
Selective breeding is a process by which organisms are chosen by humans for their desired characteristics (eg. creamy milk, large yields), and bred together to increase the frequency and magnitude of those characteristics. **mention multiple generations, human choice**

#### d
 The students should divide the space up into a coordinate system (eg. poles every 1m for 15m, in a square), and use a random number generator to pick some coordinates to survey. Then, in that quadrat picked, the students need to measure the biomass in that area. The students should repeat this process for at least 5 squares. Once they are done, they can find the average biomass in the areas they surveyed, and multiply that by the total number of areas they could survey (eg, they surveyed 5 1m<sup>2</sup> areas with biomass of 1kg, 2kg, 3kg, 4kg, 5kg, so an average biomass of 3kg. The total area was 15mx15m so 225m<sup>2</sup>, so 3\*225 = 675kg of biomass).