---
tags: ["Revision", "Biology", "Biology/Reproduction&Inheritance", "Biology/Reproduction&Inheritance/Inheritance", "TopicQuestions"]
subject: Biology
topic: "Reproduction and Inheritance"
year: U5
revision: true
marked: true
---

## 1
#### a
BB, bb
Bb

#### b
| \     | B   | b   |
| ----- | --- | --- |
| **B** | BB  | Bb  |
| **b** | bB  | bb  |

#### c
sperm, zygosis **fertilisation**, zygote, full **diploid**, mitosis, 40


## 2
#### a
###### i
47
###### i
Male

#### b
Typically, humans only have 2 sex chromosomes rather than 3.
? **extra chromosome is Y**

#### c
###### i
Meiosis
###### ii
??

**1) failure of chromosomes to separate
2) (gamete) has an extra chromosome
3) normal egg/gamete fertilised by abnormal sperm/gamete**


## 3
#### a
Mm mm
?? **M m  m**
Mm mm  mm mm **just unique combos - no need for extra mm or no**
marfan no  no no

#### b
###### i
The condition occurs in the connective tissues, so might be hard to see.
###### ii
A genetic test, seeing if parents had it. **or combined symptoms**

#### c
?? **present in offspring but not parents, skips generations, carriers present**


## 4
#### a
DD
homozygous recessive 5
6

#### b
1, 0.75


## 5
 - Looking for change in leaf growth in trees
 - Needs to be the same species of tree, in the same location, same age, no diseases.
 - The students need to survey at least 25 trees, to see the different colours.
 - They should then repeat the same experiment in at least 5 locations, with 5 species of tree.
 - They need to have a repeatable way to measure the colour - hold a torch to it, take a phone photo, check the RGB value.
**almost there, need to mention extraction**
**no need to say diff locations/species**


## 6
#### a
?? To get to the sheep? **source of food/nutrients, smell**

#### b
 - Identify sheep with bare leg/backsides
 - Breed them with each other
 - Breed children sheep with bare legs/backsides
 - Repeat over many generations

#### c
1) Nat S is best for animals
2) Nat S is much slower

#### d
1) Can develop resistances
2) Expensive