---
tags: ["Exams/Feedback", "DesignEngineering", "Exams"]
subject: Design Engineering
year: U5
---

 - Make sure to link points back to context and develop - not just *can be printed onto*, but *can be printed onto to show product information*
 - Sustainability - make sure to use the 6 Rs. Use seperate Rs for each point, not just the same thing x3, even if they are seperate explanations. Same as Latin.
 - 6 marks, 3 points - make 3 points and develop them
 - Biopolymers are made from renewable materials - **not biodegradable**. Can be made from *renewable* oils like Vegetable Oil.
 - Make sure to have clear relevant examples with explanations. 
 - For longer questions, try to have an introduction (eg. we've been focusing on X recently, we can do this by Y, here are examples.)
 - For Revision, write down a list of key topics, and think of key examples that could go for each topic, so when it comes to the exam, all is fine.
 - **mention *Mechanical Advantage***
 - Wood is non-toxic, and can be sanded down and re-oiled.

![[Scales of Manufacture]]