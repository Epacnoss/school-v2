---
tags: ["Revision", "Biology", "Biology/UseOfBiologicalResources", "Biology/StructureAndFunctionInLivingOrganisms", "Biology/StructureAndFunctionInLivingOrganisms/Excretion", "TopicQuestions"]
subject: Biology
topic: ["Use of Biological Resources", "Structure and Function in Living Organisms"]
year: U5
revision: true
marked: true
---

[[kidney-and-eye.pdf]]

## 1
#### a
Respiration produces heat, and if the oryx want to perform work they must respire in order to produce ATP. When the temperature is high, the oryx must pant in order to reduce its internal temperature so that their enzymes do not denature. So, if they are less active in summer, they sweat less and waste less water which is uncommon. This helps them to avoid dehydration.

#### b
Trees provide shade, so the areas under trees are cooler, which reduces sweating and dehydration. If they feed at night, they can respire and produce heat with less worry as the ambient temperature is lower.

#### c
Licking condensed water from plants.

#### d
This is done through the hormone ADH - Anti-Diuretic Hormone. As the blood is filtered in the kidney, just before going to the ureter, it passes through the collecting duct. Here, water is re-absorbed. The amount of water released is controlled by the amount of ADH present. If more ADH is released, the collecting duct becomes more permeable, so more water is re-absorbed and so the urine becomes more concentrated.


## 2
#### a
###### i
The hypothalamus. **or pituitary - produced in h, released in p**
###### ii
ADH makes the collecting duct more permeable, so more water is re-absorbed and the urine is more concentrated.

#### b
Nerve communications are instantaneous, but hormonal communications take a while to take effect.
Nerve communications travel via nerve pathways and electricity, but hormonal communications travel via chemicals in the blood.

#### c
![[Kidney + Eye 2.c.png]]

#### d
Roots have a positive geotropic response - that is they grow towards gravity. This allows the roots to grow deep into the soil to get access to more water and keeps the plant more stable in the soil.


## 3
#### a
###### i
D
###### ii
B

#### b
A detached retina could cause blind spots or warped vision 
**because if we change the distance between the front and the back of the eye, the light will not be focused - so the image will be blurry, and partial/full blindess**