# One-Off
Took a while to measure out holes etc.
Single unique product from start to finish.
Specific to the customer.
Small demand, high skill/labour.
High worker satisfaction.
Low initial investment.

# Batch
They had an assembly line, which saved a lot of time, but if one person made a mistake, everyone was screwed.
Hopefully, if you have skilled workers repeating the same tasks, you have fewer mistakes.

Small batch is around 100, Large batch is around a few thousand.

Flexible - wide range of products produced. Can react to demand levels, and less skilled than one-off.
Moderate capital investment for things like jigs, and workers can work on more than one process.

# Mass
Try to have a draft angle for injection moulding.
For polymers, check wall thicknesses to avoid shrinkage marks when the material is too thick - that is why shells are needed.
Injection mould takes around 3mm max to avoid them.

High vol mass production is tens of thousands to millions.

Continuous flow production, regular high demand for product, no real skill, low workforce satisfaction, high capital investment, need control/maintenance teams.