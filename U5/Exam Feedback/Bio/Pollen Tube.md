>After a pollen grain lands on the stigma of a flower, a pollen tube grows.
>Explain the role of a pollen tube (3)

 - Tube grows down style
 - Tube enters the ovary/ovule
 - Tube enters via micropyle
 - Transports nucleus/male gamete to ovary/ovule/down style
 - idea of fertilisation