---
tags: ["Exams/Feedback", "Biology", "Exams"]
subject: Biology
year: U5
---


 - If it says *use this information*, use the graph.
 - Single Cell -> Embryo: **divides by mitosis**. 2 free marks.

![[Genes]]
![[Menustral Cycle]]
![[Pollen Tube]]
![[Seeing Small Things]]
![[SMASH IT]]