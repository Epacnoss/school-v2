---
tags: ["SF", "DesignEngineering", "DesignEngineering/MaterialConsideration"]
---

> Critically examine a product and how it has developed over the last 20 yrs.
> Focusing on how designers have considered the environment in the product material choice. Describe the changes the product has been through and the impact it has had on the product/material/consumer/environment?
> 
> A3 Page

I'm doing Billiard Balls at: https://www.canva.com/design/DAFLXsz1SxQ/EcCjXGC3wq3OBnIhGhtPYA/view?utm_content=DAFLXsz1SxQ&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton


Make sure to use [[Environmental Considerations#The 6 Rs]]