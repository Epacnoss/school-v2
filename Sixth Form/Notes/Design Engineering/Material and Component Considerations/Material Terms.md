---
tags: ["SF", "DesignEngineering", "DesignEngineering/MaterialConsideration"]
---

Working from a board list

Linked to [[Properties & Characteristics]]

| Term                 | Definition                               |
| -------------------- | ---------------------------------------- |
| Hardness             | Resistance to scratching - surface layer |
| Impact Resistance    | Resistance to deformation upon impact    |
| Malleability         | Ability to be stretched into sheets      |
| Corrosion Resistance | Resistance to chemical erosion           |
| Plasticity           | Capacity to be moulded or changed shape  |
| Ductility            | Ability to be stretched into a wire      |
| Deciduous            | Loses leaves in winter                   |
| Coniferous           | Doesn't lose leaves in winter            |
| Composite            | Lots of materials                        |
| Non-ferrous          | Contains no Iron (Fe)                    |
