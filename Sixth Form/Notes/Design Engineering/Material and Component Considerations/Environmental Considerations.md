---
tags: ["SF", "DesignEngineering", "DesignEngineering/MaterialConsideration"]
---

 - Where are the materials from?
 - Pollution levels in production and transportation
 - Limiting finite resources in the selection of materials

# The 6 Rs
 - Rethink
 - Reduce
 - Reuse
 - Repair
 - Recycle
 - Refuse