---
tags: ["SF", "DesignEngineering", "DesignEngineering/MaterialConsideration"]
---

# Functional Performance
Refers to how it actually works and performs, eg. weight, durability, elasticity.
Every product will have a crucial criteria, usually established during the development stage, that the material selection will usually depend on.
For example, a product must support a specific load.

# Aesthetics
Within its widest interpretation, aesthetics is involved with our senses. The emotional response it evokes through the vision, feel, etc.

# Form vs Function
Which takes priority follows industry norms (eg. fashion vs cars).