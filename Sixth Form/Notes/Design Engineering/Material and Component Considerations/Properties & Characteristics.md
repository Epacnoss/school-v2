---
tags: ["SF", "DesignEngineering", "DesignEngineering/MaterialConsideration"]
---

# Categories

From a list on the board

## Our Guesses
### Mechanical Properties
 - Flexibility
 - Formability
 - Elasticity
 - Coefficient of Friction
 - Acoustic Properties

### Physical Properties
 - Ductility
 - Density
 - Conductivity
 - Strength
 - Toughness
 - Hardness

### Manufacturing Properties
 - Fatigue Limit
 - Machinability

### Chemical Properties
 - Corrosion Resistance
 - Flammability
 - Thermal Expansion
 - Hygroscopy
 - Fusibility

## Actual
### Mechanical Properties
 - Strength
 - Ductility
 - Hardness
 - Toughness
 - Elasticity
 - Flexibility
 - Fatigue Limit
 - Coefficient of Friction

### Physical Properties
 - Density
 - Conductivity
 - Melting Point
 - Flammability
 - Thermal Expansion
 - Acoustic Properties

### Chemical Properties
 - Corrosion Resistance
 - Reactivity
 - Hygroscopy

### Manufacturing Property
 - Formability
 - Machinability
 - Fusibility
