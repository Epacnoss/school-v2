---
tags: ["SF", "DesignEngineering", "DesignEngineering/MaterialConsideration"]
---

 - Transportation - Where are the raw materials located? Where are the materials produced? Are pre-prepared parts made in off shore? What sort of transportation is required? etc.
 - Processing of materials & components
 - Considering pre-prepared component parts
 - Material finishing or pre-coated rather than in house can change the final costing
 - Specialist machinery required for material choices & off shore cheap manufacturing
 - Standard sizes and parts
 - Ensuring parts meet appropriate standards
 - Supply & Demand