---
tags: ["SF", "DesignEngineering"]
---

# Exams
 - Principles - 26.7% (80 mks)
 - Problem Solving - 23.3% (70 mks)
 - NEA - 50% (150 mks)

## Principles 
See GCSE - same

## Iterative Design Challenge
get wrck'd lol


## Problem Solving
Sets a challenge, comprehension, solve the problem. Weird paper, weird technique, but should be good eventually. Revise same material as [[DE Spec#Principles]]]

# Timing

| Period     | Exam                                           |
| ---------- | ---------------------------------------------- |
| Sept '22   | Start of course, begin theory/projects         |
| March '23  | Start [[DE Spec#Iterative Design Challenge]]   |
| Jan '24    | Mock Exams                                     |
| Easter '24 | Hand in [[DE Spec#Iterative Design Challenge]] |
| Summer '24 | Final Exam                                     |

