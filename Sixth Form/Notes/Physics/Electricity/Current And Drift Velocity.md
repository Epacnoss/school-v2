---
tags: ["SF", "Physics", "Physics/Electricity"]
---

$I = \frac{\Delta Q}{\Delta t}, V = \frac{E}{Q}, V=IR$

# What actually drains electrons round the circuit?
When a complete conductive loop is made from positive to negative there is an electric field that passes through the wire. This has the effect of producing a force on charge carriers, moving the delocalised electrons in metals. All electrons move together immediately.
![[Electrons relating to A, C and I.png|200]]
![[Electrons passing through an electric field.png|200]]
Electrons are our charge carriers in electric circuits. They have a charge of $-1.6*10^{-19}C$. For example, when *charging* the Van de Graff generator for 10s, it gained a charge of $304 nC$. We can divide the charge by the magnitude of one electron, and then divide that by the time taken to get the number of electrons that were transferred to the Van de Graff generator: $1.9*10^{12}$.