---
tags: ["SF", "Physics", "Physics/Electricity"]
---

Working from [[W1 GCSE Electricity recap quiz.pdf]]

# Easy
## 1
The flow of electrons  **rate of flow of charge - close but not quite, also: $I=\frac{Q}{t}$**

## 2
$V = IR$

## 3
$P = IV = 5 * 2 = 10W$ 

## 4
![[Filament Lamp Resistance.png|200]]

## 5
![[LED Circuit Symbol.png|200]]


# Getting Harder
## 1
The number of joules per coulomb **or energy transferred over number => $V = \frac{E}{Q}$**

## 2
Coulomb 

## 3
?? **In a closed circuit, resistance is directly proportional to current**

## 4
![[Metal Wire Resistance Graphs.png|200]]

## 5
$V_1 = 6V, V_2 = 6V$


# A bit difficult?
## 1
$R_{total} = 5 + 1 = 6\ohm$
$V_1 = 12 * \frac{1}{6} = 2V$
$V_2 = 12 * \frac{5}{6} = 10V$

## 2
$V = IR, I = V/R$
$I_1 = 6 / 2 = 3A$
$I_2 = 6/3 = 2A$

## 3
No change as measuring from fixed resistor.
**no - resistance in whole circuit changes, so gets lower, because LDR gets more voltage so Fixed Resistor gets less voltage**

## 4
![[Graphed Circuit.png|200]]


# Extension
## 1
### a
$3V$
### b
$R_{total} = 1 + 1 = 2\ohm$
$V = 3 * \frac{1}{2} = 1.5V$
### c
$1 / R_{total} = \frac{1}{R_1} + \frac{1}{R_2} + \ldots$
$1/x = 1/3 + 1/2 = 5/6$
$x = 6/5 = 1.2\ohm$

