---
tags: ["SF", "Physics", "Physics/Electricity"]
---

![[Van Der Graaf.png|200]]
 - Charge builds up due to friction depositing electrons which are carried into the dome. A charge builds up in the dome.
 - Charge is located in the dome as it is a conductor and the electrons (which repel each other) eventually spread out evenly all over the dome.
 - As a conductor touches the dome, it receives an electric shock (AKA the dome discharges into the conductor), as that conductor provides a path to ground for the electrons. This can also be explained in another way: the electrons move from the area of low potential (the dome) to an area of high potential (the conductor, which could be a finger or a metal rod, etc.)